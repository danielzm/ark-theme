module.exports = function ArkThemeModule(pb) {

    /**
     * ArkTheme - A theme plugin for PencilBlue
     *
     * @author Daniel Zamorano @danielzm <daniel.zamorano.m@gmail.com>
     * @copyright 2015 Daniel Zamorano
     */

    function ArkTheme() {}

    ArkTheme.onInstall = function (cb) {
        var self = this;
        cb(null, true);
        return;
    };

    ArkTheme.onUninstall = function (cb) {
        cb(null, true);
    };

    ArkTheme.onStartup = function (cb) {
        pb.AdminSubnavService.registerFor('plugin_settings', function (navKey, localization, data) {
            if (data.plugin.uid === 'private_areas') {
                return [{
                    name: 'home_page_settings',
                    title: 'Home page settings',
                    icon: 'home',
                    href: '/admin/plugins/private_areas/settings/home_page'
                }];
            }
            return [];
        });
        cb(null, true);
    };
    ArkTheme.onShutdown = function (cb) {
        cb(null, true);
    };

    return ArkTheme;
};
